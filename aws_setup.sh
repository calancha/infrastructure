# shellcheck shell=bash
# not executable as this file should be sourced for the export/unset to have any effect

# without a credentials file, use the deployment service account
if ! [ -f ~/.aws/credentials ]; then
    # shellcheck disable=SC2154
    export AWS_ACCESS_KEY_ID=${DEPLOYMENT_AWS_ACCESS_KEY_ID}
    # shellcheck disable=SC2154
    export AWS_SECRET_ACCESS_KEY=${DEPLOYMENT_AWS_SECRET_ACCESS_KEY}
    unset AWS_PROFILE
fi
